/**
 * © Concise Computing
 * CStrap - Development Framework
 * https://strap.concise.cc
 *
**/

for (let lt = 0; lt < 26 ; lt++) {
  window[String.fromCharCode(97 + parseInt(lt))] = undefined;
}

const qs = (e) => { return document.querySelector(e); }
const qsa = (e) => { return document.querySelectorAll(e); }

const qsc = (e) => { return document.querySelector(`[class*="${e}"]`); }
const qsac = (e) => { return document.querySelectorAll(`[class*="${e}"]`); }

document.addEventListener('DOMContentLoaded', () => {
  setCols(); setSizes();
});

const bg = qsac("bg-"),
      fg = qsac("fg-");

const mxpx = qsac("mx-"),
      mxps = qsac("mx"),
      mypx = qsac("my-"),
      myps = qsac("my"),
      pxpx = qsac("px-"),
      pxps = qsac("px"),
      pypx = qsac("py-"),
      pyps = qsac("py"),
      mlpx = qsac("ml-"),
      mlps = qsac("ml"),
      mrpx = qsac("mr-"),
      mrps = qsac("mr"),
      plpx = qsac("pl-"),
      plps = qsac("pl"),
      prpx = qsac("pr-"),
      prps = qsac("pr"),
      mtpx = qsac("mt-"),
      mtps = qsac("mt"),
      mbpx = qsac("mb-"),
      mbps = qsac("mb"),
      ptpx = qsac("pt-"),
      ptps = qsac("pt"),
      pbpx = qsac("pb-"),
      pbps = qsac("pb"),
      mpx = qsac("m-"),
      mps = qsac("m"),
      ppx = qsac("p-"),
      pps = qsac("p"),
      wpx = qsac("w-"),
      wps = qsac("w"),
      hpx = qsac("h-"),
      hps = qsac("h"),
      nmxpx = qsac("nmx-"),
      nmxps = qsac("nmx"),
      nmypx = qsac("nmy-"),
      nmyps = qsac("nmy"),
      npxpx = qsac("npx-"),
      npxps = qsac("npx"),
      npypx = qsac("npy-"),
      npyps = qsac("npy"),
      nmlpx = qsac("nml-"),
      nmlps = qsac("nml"),
      nmrpx = qsac("nmr-"),
      nmrps = qsac("nmr"),
      nplpx = qsac("npl-"),
      nplps = qsac("npl"),
      nprpx = qsac("npr-"),
      nprps = qsac("npr"),
      nmtpx = qsac("nmt-"),
      nmtps = qsac("nmt"),
      nmbpx = qsac("nmb-"),
      nmbps = qsac("nmb"),
      nptpx = qsac("npt-"),
      nptps = qsac("npt"),
      npbpx = qsac("npb-"),
      npbps = qsac("npb"),
      nmpx = qsac("nm-"),
      nmps = qsac("nm"),
      nppx = qsac("np-"),
      npps = qsac("np"),
      nwpx = qsac("nw-"),
      nwps = qsac("nw"),
      nhpx = qsac("nh-"),
      nhps = qsac("nh");


const setCols = () => {

  bg.forEach((e) => { try {
    	c = e.className.match(/bg-([a-z0-9]+)/)[1];
    	e.style.backgroundColor = `#${c}`;
		} catch { /* No match detected */ }
	});

  fg.forEach((e) => { try {
    	c = e.className.match(/fg-([a-z0-9]+)/)[1];
    	e.style.color = `#${c}`;
		} catch { /* No match detected */ }
	});

};

const setSizes = () => {

  // Main

  mxpx.forEach((e) => { try {
    	c = e.className.match(/mx-([0-9]+)/)[1];
    	e.style.marginLeft = `${c}px`;
    	e.style.marginRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  mxps.forEach((e) => { try {
    	c = e.className.match(/mx([0-9]+)/)[1];
    	e.style.marginLeft = `${c}%`;
    	e.style.marginRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  mypx.forEach((e) => { try {
    	c = e.className.match(/my-([0-9]+)/)[1];
    	e.style.marginTop = `${c}px`;
    	e.style.marginBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  myps.forEach((e) => { try {
    	c = e.className.match(/my([0-9]+)/)[1];
    	e.style.marginTop = `${c}%`;
    	e.style.marginBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  pxpx.forEach((e) => { try {
    	c = e.className.match(/px-([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}px`;
    	e.style.paddingRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  pxps.forEach((e) => { try {
    	c = e.className.match(/px([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}%`;
    	e.style.paddingRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  pypx.forEach((e) => { try {
    	c = e.className.match(/py-([0-9]+)/)[1];
    	e.style.paddingTop = `${c}px`;
    	e.style.paddingBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  pyps.forEach((e) => { try {
    	c = e.className.match(/py([0-9]+)/)[1];
    	e.style.paddingTop = `${c}%`;
    	e.style.paddingBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  mlpx.forEach((e) => { try {
    	c = e.className.match(/ml-([0-9]+)/)[1];
    	e.style.marginLeft = `${c}px`;
		} catch { /* No match detected */ }
	});

  mlps.forEach((e) => { try {
    	c = e.className.match(/ml([0-9]+)/)[1];
    	e.style.marginLeft = `${c}%`;
		} catch { /* No match detected */ }
	});

  mrpx.forEach((e) => { try {
    	c = e.className.match(/mr-([0-9]+)/)[1];
    	e.style.marginRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  mrps.forEach((e) => { try {
    	c = e.className.match(/mr([0-9]+)/)[1];
    	e.style.marginRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  plpx.forEach((e) => { try {
    	c = e.className.match(/pl-([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}px`;
		} catch { /* No match detected */ }
	});

  plps.forEach((e) => { try {
    	c = e.className.match(/pl([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}%`;
		} catch { /* No match detected */ }
	});

  prpx.forEach((e) => { try {
    	c = e.className.match(/pr-([0-9]+)/)[1];
    	e.style.paddingRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  prps.forEach((e) => { try {
    	c = e.className.match(/pr([0-9]+)/)[1];
    	e.style.paddingRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  mtpx.forEach((e) => { try {
    	c = e.className.match(/mt-([0-9]+)/)[1];
    	e.style.marginTop = `${c}px`;
		} catch { /* No match detected */ }
	});

  mtps.forEach((e) => { try {
    	c = e.className.match(/mt([0-9]+)/)[1];
    	e.style.marginTop = `${c}%`;
		} catch { /* No match detected */ }
	});

  mbpx.forEach((e) => { try {
    	c = e.className.match(/mb-([0-9]+)/)[1];
    	e.style.marginBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  mbps.forEach((e) => { try {
    	c = e.className.match(/mb([0-9]+)/)[1];
    	e.style.marginBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  ptpx.forEach((e) => { try {
    	c = e.className.match(/pt-([0-9]+)/)[1];
    	e.style.paddingTop = `${c}px`;
		} catch { /* No match detected */ }
	});

  ptps.forEach((e) => { try {
    	c = e.className.match(/pt([0-9]+)/)[1];
    	e.style.paddingTop = `${c}%`;
		} catch { /* No match detected */ }
	});

  pbpx.forEach((e) => { try {
    	c = e.className.match(/pb-([0-9]+)/)[1];
    	e.style.paddingBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  pbps.forEach((e) => { try {
    	c = e.className.match(/pb([0-9]+)/)[1];
    	e.style.paddingBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  mpx.forEach((e) => { try {
    	c = e.className.match(/m-([0-9]+)/)[1];
    	e.style.margin = `${c}px`;
    } catch { /* No match detected */ }
  });

  mps.forEach((e) => { try {
      c = e.className.match(/m([0-9]+)/)[1];
      e.style.margin = `${c}%`;
    } catch { /* No match detected */ }
  });

  ppx.forEach((e) => { try {
      c = e.className.match(/p-([0-9]+)/)[1];
      e.style.padding = `${c}px`;
    } catch { /* No match detected */ }
  });

  pps.forEach((e) => { try {
      c = e.className.match(/p([0-9]+)/)[1];
      e.style.padding = `${c}%`;
    } catch { /* No match detected */ }
  });


  // Width & Height

  wpx.forEach((e) => { try {
      c = e.className.match(/w-([0-9]+)/)[1];
      e.style.width = `${c}px`;
    } catch { /* No match detected */ }
  });

  wps.forEach((e) => { try {
      c = e.className.match(/w([0-9]+)/)[1];
      e.style.width = `${c}%`;
    } catch { /* No match detected */ }
  });

  hpx.forEach((e) => { try {
      c = e.className.match(/h-([0-9]+)/)[1];
      e.style.height = `${c}px`;
    } catch { /* No match detected */ }
  });

  hps.forEach((e) => { try {
      c = e.className.match(/h([0-9]+)/)[1];
      e.style.height = `${c}%`;
    } catch { /* No match detected */ }
  });


  // Negative Values

  nmxpx.forEach((e) => { try {
    	c = e.className.match(/nmx-([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}px`;
    	e.style.marginRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmxps.forEach((e) => { try {
    	c = e.className.match(/nmx([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}%`;
    	e.style.marginRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmypx.forEach((e) => { try {
    	c = e.className.match(/nmy-([0-9]+)/)[1];
    	e.style.marginTop = `-${c}px`;
    	e.style.marginBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmyps.forEach((e) => { try {
    	c = e.className.match(/nmy([0-9]+)/)[1];
    	e.style.marginTop = `-${c}%`;
    	e.style.marginBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  npxpx.forEach((e) => { try {
    	c = e.className.match(/npx-([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}px`;
    	e.style.paddingRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  npxps.forEach((e) => { try {
    	c = e.className.match(/npx([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}%`;
    	e.style.paddingRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  npypx.forEach((e) => { try {
    	c = e.className.match(/npy-([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}px`;
    	e.style.paddingBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  npyps.forEach((e) => { try {
    	c = e.className.match(/npy([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}%`;
    	e.style.paddingBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmlpx.forEach((e) => { try {
    	c = e.className.match(/nml-([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmlps.forEach((e) => { try {
    	c = e.className.match(/nml([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmrpx.forEach((e) => { try {
    	c = e.className.match(/nmr-([0-9]+)/)[1];
    	e.style.marginRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmrps.forEach((e) => { try {
    	c = e.className.match(/nmr([0-9]+)/)[1];
    	e.style.marginRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nplpx.forEach((e) => { try {
    	c = e.className.match(/npl-([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nplps.forEach((e) => { try {
    	c = e.className.match(/npl([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nprpx.forEach((e) => { try {
    	c = e.className.match(/npr-([0-9]+)/)[1];
    	e.style.paddingRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nprps.forEach((e) => { try {
    	c = e.className.match(/npr([0-9]+)/)[1];
    	e.style.paddingRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmtpx.forEach((e) => { try {
    	c = e.className.match(/nmt-([0-9]+)/)[1];
    	e.style.marginTop = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmtps.forEach((e) => { try {
    	c = e.className.match(/nmt([0-9]+)/)[1];
    	e.style.marginTop = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmbpx.forEach((e) => { try {
    	c = e.className.match(/nmb-([0-9]+)/)[1];
    	e.style.marginBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmbps.forEach((e) => { try {
    	c = e.className.match(/nmb([0-9]+)/)[1];
    	e.style.marginBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nptpx.forEach((e) => { try {
    	c = e.className.match(/npt-([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nptps.forEach((e) => { try {
    	c = e.className.match(/npt([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}%`;
		} catch { /* No match detected */ }
	});

  npbpx.forEach((e) => { try {
    	c = e.className.match(/npb-([0-9]+)/)[1];
    	e.style.paddingBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  npbps.forEach((e) => { try {
    	c = e.className.match(/npb([0-9]+)/)[1];
    	e.style.paddingBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmpx.forEach((e) => { try {
    	c = e.className.match(/nm-([0-9]+)/)[1];
    	e.style.margin = `-${c}px`;
    } catch { /* No match detected */ }
  });

  nmps.forEach((e) => { try {
      c = e.className.match(/nm([0-9]+)/)[1];
      e.style.margin = `-${c}%`;
    } catch { /* No match detected */ }
  });

  nppx.forEach((e) => { try {
      c = e.className.match(/np-([0-9]+)/)[1];
      e.style.padding = `-${c}px`;
    } catch { /* No match detected */ }
  });

  npps.forEach((e) => { try {
      c = e.className.match(/np([0-9]+)/)[1];
      e.style.padding = `-${c}%`;
    } catch { /* No match detected */ }
  });


  // Negative Width & Height

  nwpx.forEach((e) => { try {
      c = e.className.match(/nw-([0-9]+)/)[1];
      e.style.width = `-${c}px`;
    } catch { /* No match detected */ }
  });

  nwps.forEach((e) => { try {
      c = e.className.match(/nw([0-9]+)/)[1];
      e.style.width = `-${c}%`;
    } catch { /* No match detected */ }
  });

  nhpx.forEach((e) => { try {
      c = e.className.match(/nh-([0-9]+)/)[1];
      e.style.height = `-${c}px`;
    } catch { /* No match detected */ }
  });

  nhps.forEach((e) => { try {
      c = e.className.match(/nh([0-9]+)/)[1];
      e.style.height = `-${c}%`;
    } catch { /* No match detected */ }
  });


};



