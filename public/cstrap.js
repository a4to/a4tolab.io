/**
 * File: cstrap.js
 * CStrap - Development Framework
 * https://strap.concise.cc
 * © Concise Computing
 *
**/

for (let lt = 0; lt < 26 ; lt++) {
  window[String.fromCharCode(97 + parseInt(lt))] = undefined;
}

const qs = (e) => { return document.querySelector(e); }
const qsa = (e) => { return document.querySelectorAll(e); }

const qsc = (e) => { return document.querySelector(`[class*="${e}"]`); }
const qsac = (e) => { return document.querySelectorAll(`[class*="${e}"]`); }

document.addEventListener('DOMContentLoaded', () => {
  setCols(); setSizes(); setStyles();
});

const setCols = () => {

  let cols = [], singles = [];
  Object.keys(colors.main).forEach((key) => { cols.push(key); });
  Object.keys(colors.singles).forEach((key) => { singles.push(key); });

  for (let i = 0; i < cols.length; i++) {
    window[cols[i]] = colors.main[cols[i]].main;
  }

  bg.forEach((e) => { try {
      c = e.className.match(/bg-([a-z0-9\-]+)/)[1];
      n = c.indexOf("-") > -1 ? c.split("-").length - 1 : 0;
      if (cols.includes(c) && n == 0) { e.style.backgroundColor = window[c]; }
      else if (singles.includes(c)) { e.style.backgroundColor = colors.singles[c]; }
      else if (c.indexOf("-") > 0) {
        c = c.split("-");
        if (cols.includes(c[0]) && n == 1) { e.style.backgroundColor = colors.main[c[0]].shades[c[1]]; }
        else if (cols.includes(c[0]) && n == 2) {
          e.style.backgroundColor = window[c[0]];
          e.style.backgroundImage = `linear-gradient(to ${c[1]}, ${window[c[0]]}, ${window[c[2]]})`;
        }
      } else { e.style.backgroundColor = `#${c}`; }
    } catch { /* No match detected */ }
  });

  fg.forEach((e) => { try {
      c = e.className.match(/fg-([a-z0-9\-]+)/)[1];
      n = c.indexOf("-") > -1 ? c.split("-").length - 1 : 0;
      if (cols.includes(c) && n == 0) { e.style.color = window[c]; }
      else if (singles.includes(c)) { e.style.color = colors.singles[c]; }
      else if (c.indexOf("-") > 0) {
        c = c.split("-");
        if (cols.includes(c[0]) && n == 1) { e.style.color = colors.main[c[0]].shades[c[1]]; }
        else if (cols.includes(c[0]) && n == 2) {
          e.style.color = window[c[0]];
          e.style.backgroundImage = `linear-gradient(to ${c[1]}, ${window[c[0]]}, ${window[c[2]]})`;
        }
      } else { e.style.color = `#${c}`; }
    } catch { /* No match detected */ }
  });

  shad.forEach((e) => { try {

      c = e.className.match(/shad-([a-z0-9\-]+)/)[1];
      n = c.indexOf("-") > -1 ? c.split("-").length - 1 : 0;
      c = c.split("-");
      s = c[0];

      if (n == 0) { e.style.boxShadow = `0 0 10px ${cols.includes(s) ? colors.main[s].main : `#${s}`}`; }
      else if (n == 1) { e.style.boxShadow = `0 0 ${c[1]}px ${cols.includes(s) ? colors.main[s].main : `#${s}`}`; }
      else if (n == 2) { e.style.boxShadow = `0 0 ${c[1]}px ${c[2]}px ${cols.includes(s) ? colors.main[s].main : `#${s}`}`; }
      else if (n == 3) { e.style.boxShadow = `${c[1]}px ${c[2]}px ${c[3]}px ${c[4]}px ${cols.includes(s) ? colors.main[s].main : `#${s}`}`; }
      else if (n == 4) { e.style.boxShadow = `${c[2]}px ${c[3]}px ${c[4]}px ${c[5]}px ${cols.includes(s) ? colors.main[s].shades[c[1]] : `#${s}`}`; }

    } catch { /* No match detected */ }
  });

  bd.forEach((e) => { try {

        c = e.className.match(/bd-([a-z0-9\-]+)/)[1];
        n = c.indexOf("-") > -1 ? c.split("-").length - 1 : 0;
        c = c.split("-");
        s = c[0];

        if (n == 0) { e.style.border = `solid 2px ${cols.includes(s) ? colors.main[s].main : `#${s}`}`; }
        else if (n == 1) { e.style.border = `solid ${c[1]}px ${cols.includes(s) ? colors.main[s].main : `#${s}`}`; }
        else if (n == 2) { e.style.border = `solid ${c[2]}px ${cols.includes(s) ? colors.main[s].shades[c[1]] : `#${s}`}`; }

      } catch { /* No match detected */ }
  });

}

const setStyles = () => {

  op.forEach((e) => { try {
      c = e.className.match(/o-([a-z0-9]+)/)[1];
      e.style.opacity = `.${c}`;
    } catch { /* No match detected */ }
  });

  blr.forEach((e) => { try {
      c = e.className.match(/blr-([a-z0-9]+)/)[1];
      e.style.filter = `blur(${c}px)`;
    } catch { /* No match detected */ }
  });

  hsla.forEach((e) => { try {
      c = e.className.match(/hsla-([a-z0-9\-]+)/)[1];
      d = c.split("-");
      e.style.backgroundColor = `hsla(${d[0]}, ${d[1]}%, ${d[2]}%, 0.${d[3]})`;
    } catch { /* No match detected */ }
  });

  rgba.forEach((e) => { try {
      c = e.className.match(/rgba-([a-z0-9\-]+)/)[1];
      d = c.split("-");
      e.style.backgroundColor = `rgba(${d[0]}, ${d[1]}, ${d[2]}, 0.${d[3]})`;
    } catch { /* No match detected */ }
  });

  center.forEach((e) => { try {
      c = e.className.match(/c-([a-z0-9]+)/)[1];
      e.style.width = `calc(100% - ${2 * c}px)`;
      e.style.height = `calc(100% - ${2 * c}px)`;
      e.style.margin = `${c}px`;
      e.style.position = `absolute`;
    } catch { /* No match detected */ }
  });

  br.forEach((e) => { try {
      c = e.className.match(/br-([a-z0-9]+)/)[1];
      e.style.borderRadius = `${c}px`;
    } catch { /* No match detected */ }
  });

  g.forEach((e) => { try {
      c = e.className.match(/g-([a-z0-9\-]+)/)[1];
      e.style.gap = `${c}px`;
    } catch { /* No match detected */ }
  });

  invert.forEach((e) => { try {
      c = e.className.match(/invert-([a-z0-9]+)/)[1];
      f = e.style.color;
      b = e.style.backgroundColor;
      e.style.backgroundColor = colInvert(c);
      e.style.color = colInvert(c);
    } catch { /* No match detected */ }
  });

};

const bg = qsac("bg-"),
      fg = qsac("fg-"),
      op = qsac("o-"),
      blr = qsac("blr-"),
      hsla = qsac("hsla-"),
      rgba = qsac("rgba-"),
      center = qsac("c-"),
      br = qsac("br-"),
      shad = qsac("shad-"),
      bd = qsac("bd-"),
      g = qsac("g-"),
      invert = qsac("invert");


const colors = {
  main: {
    light: { main: "#f0f0f0", shades: [ "#ffffff", "#f8f9fa", "#e9ecef", "#dee2e6", "#ced4da", "#cccccc", "#b9b9b9", "#a6a6a6", "#939393", "#808080" ] },
    dark: { main: "#212529", shades: [ "#000000", "#111111", "#222222", "#333333", "#444444", "#555555", "#666666", "#777777", "#888888", "#999999" ] },
    purple: { main: "#686aff", shades: [ "#bf00ff", "#9f00e0", "#7f00bf", "#5f009f", "#3f007f", "#5f3dc4", "#4e2fa2", "#3d1f81", "#2c105f", "#1b083f" ] },
    teal: { main: "#20c997", shades: [ "#00ffbf", "#00e5a3", "#00cc87", "#00b26b", "#00994f", "#008033", "#00661a", "#004c00", "#003300", "#001900" ] },
    green: { main: "#00dd00", shades: [ "#00ff00", "#00e000", "#00bf00", "#00aa00", "#009900", "#008800", "#007700", "#006600", "#005500", "#004400" ] },
    red: { main: "#ff0000", shades: [ "#ff0000", "#e00000", "#bf0000", "#aa0000", "#990000", "#880000", "#770000", "#660000", "#550000", "#440000" ] },
    orange: { main: "#FF5E0E", shades: [ "#FF4500", "#FF4F00", "#FF6E00", "#FF7E00", "#FF9400", "#FFA400", "#FFB400", "#FFC400", "#FFD400", "#FFE400" ] },
    yellow: { main: "#ffc107", shades: [ "#ffff00", "#ffe500", "#ffcc00", "#ffb200", "#ff9900", "#ff8000", "#e67300", "#cc6600", "#b35900", "#994c00" ] },
    blue: { main: "#007bff", shades: [ "#00aaff", "#0099ff", "#0088ff", "#0077ff", "#0066ff", "#0055ff", "#0044ff", "#0033ff", "#0022ff", "#0011ff" ] },
    cyan: { main: "#00ffff", shades: [ "#00ffff", "#00e5e5", "#00cccc", "#00b2b2", "#17a2b8", "#0099ff", "#0080ff", "#0066ff", "#004cff", "#0033ff" ] },
    pink: { main: "#ff00ff", shades: [ "#ff00ff", "#e000e0", "#bf00bf", "#aa00aa", "#990099", "#880088", "#770077", "#660066", "#550055", "#440044" ] },
    brown: { main: "#8b4513", shades: [ "#a0522d", "#8b4513", "#7a3b12", "#6b3211", "#5c2910", "#4d200f", "#3e170e", "#2f0e0d", "#20050c", "#110c0b" ] },
    grey: { main: "#808080", shades: [ "#808080", "#737373", "#666666", "#595959", "#4c4c4c", "#3f3f3f", "#323232", "#252525", "#181818", "#0b0b0b" ] },
    white: { main: "#ffffff", shades: [ "#ffffff", "#f8f8f8", "#f0f0f0", "#e8e8e8", "#e0e0e0", "#d8d8d8", "#d0d0d0", "#c8c8c8", "#c0c0c0", "#b8b8b8" ] },
    black: { main: "#000000", shades: [ "#000000", "#080808", "#101010", "#181818", "#202020", "#282828", "#303030", "#383838", "#404040", "#484848" ] }
  },
  singles: {
    milk: "#e8eaec",
    snow: "#f9fafb",
  }
};

d = [
  { purp: colors.main.purple },
  { indigo: colors.main.purple }
];

d.forEach((e) => {
  Object.keys(e).forEach((f) => {
    colors.main[f] = e[f];
  });
});

const hexToRgb = (hex) => {
  if (hex.length === 6) {
    r = parseInt(hex.slice(0, 2), 16);
    g = parseInt(hex.slice(2, 4), 16);
    b = parseInt(hex.slice(4, 6), 16);
    l = hex.slice(0, 7);
  } else if (hex.length === 3) {
    r = parseInt(hex.slice(0, 1) + hex.slice(0, 1), 16);
    g = parseInt(hex.slice(1, 2) + hex.slice(1, 2), 16);
    b = parseInt(hex.slice(2, 3) + hex.slice(2, 3), 16);
  } else {
     return null;
  }
  return [r, g, b];
};

const rgbToHex = (rgb) => {
  r = rgb[0].toString(16);
  g = rgb[1].toString(16);
  b = rgb[2].toString(16);
  return `#${r}${g}${b}`;
};

const invertCols = (hex) => {
  const rgb = hexToRgb(hex);
  r = 255 - rgb[0];
  g = 255 - rgb[1];
  b = 255 - rgb[2];
  return rgbToHex([r, g, b]);
};

const mxpx = qsac("mx-"),
      mxps = qsac("mx"),
      mypx = qsac("my-"),
      myps = qsac("my"),
      pxpx = qsac("px-"),
      pxps = qsac("px"),
      pypx = qsac("py-"),
      pyps = qsac("py"),
      mlpx = qsac("ml-"),
      mlps = qsac("ml"),
      mrpx = qsac("mr-"),
      mrps = qsac("mr"),
      plpx = qsac("pl-"),
      plps = qsac("pl"),
      prpx = qsac("pr-"),
      prps = qsac("pr"),
      mtpx = qsac("mt-"),
      mtps = qsac("mt"),
      mbpx = qsac("mb-"),
      mbps = qsac("mb"),
      ptpx = qsac("pt-"),
      ptps = qsac("pt"),
      pbpx = qsac("pb-"),
      pbps = qsac("pb"),
      mpx = qsac("m-"),
      mps = qsac("m"),
      ppx = qsac("p-"),
      pps = qsac("p"),
      wpx = qsac("w-"),
      wps = qsac("w"),
      hpx = qsac("h-"),
      hps = qsac("h"),
      nmxpx = qsac("nmx-"),
      nmxps = qsac("nmx"),
      nmypx = qsac("nmy-"),
      nmyps = qsac("nmy"),
      npxpx = qsac("npx-"),
      npxps = qsac("npx"),
      npypx = qsac("npy-"),
      npyps = qsac("npy"),
      nmlpx = qsac("nml-"),
      nmlps = qsac("nml"),
      nmrpx = qsac("nmr-"),
      nmrps = qsac("nmr"),
      nplpx = qsac("npl-"),
      nplps = qsac("npl"),
      nprpx = qsac("npr-"),
      nprps = qsac("npr"),
      nmtpx = qsac("nmt-"),
      nmtps = qsac("nmt"),
      nmbpx = qsac("nmb-"),
      nmbps = qsac("nmb"),
      nptpx = qsac("npt-"),
      nptps = qsac("npt"),
      npbpx = qsac("npb-"),
      npbps = qsac("npb"),
      nmpx = qsac("nm-"),
      nmps = qsac("nm"),
      nppx = qsac("np-"),
      npps = qsac("np"),
      nwpx = qsac("nw-"),
      nwps = qsac("nw"),
      nhpx = qsac("nh-"),
      nhps = qsac("nh");




const setSizes = () => {

  mxpx.forEach((e) => { try {
    	c = e.className.match(/mx-([0-9]+)/)[1];
    	e.style.marginLeft = `${c}px`;
    	e.style.marginRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  mxps.forEach((e) => { try {
    	c = e.className.match(/mx([0-9]+)/)[1];
    	e.style.marginLeft = `${c}%`;
    	e.style.marginRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  mypx.forEach((e) => { try {
    	c = e.className.match(/my-([0-9]+)/)[1];
    	e.style.marginTop = `${c}px`;
    	e.style.marginBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  myps.forEach((e) => { try {
    	c = e.className.match(/my([0-9]+)/)[1];
    	e.style.marginTop = `${c}%`;
    	e.style.marginBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  pxpx.forEach((e) => { try {
    	c = e.className.match(/px-([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}px`;
    	e.style.paddingRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  pxps.forEach((e) => { try {
    	c = e.className.match(/px([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}%`;
    	e.style.paddingRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  pypx.forEach((e) => { try {
    	c = e.className.match(/py-([0-9]+)/)[1];
    	e.style.paddingTop = `${c}px`;
    	e.style.paddingBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  pyps.forEach((e) => { try {
    	c = e.className.match(/py([0-9]+)/)[1];
    	e.style.paddingTop = `${c}%`;
    	e.style.paddingBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  mlpx.forEach((e) => { try {
    	c = e.className.match(/ml-([0-9]+)/)[1];
    	e.style.marginLeft = `${c}px`;
		} catch { /* No match detected */ }
	});

  mlps.forEach((e) => { try {
    	c = e.className.match(/ml([0-9]+)/)[1];
    	e.style.marginLeft = `${c}%`;
		} catch { /* No match detected */ }
	});

  mrpx.forEach((e) => { try {
    	c = e.className.match(/mr-([0-9]+)/)[1];
    	e.style.marginRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  mrps.forEach((e) => { try {
    	c = e.className.match(/mr([0-9]+)/)[1];
    	e.style.marginRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  plpx.forEach((e) => { try {
    	c = e.className.match(/pl-([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}px`;
		} catch { /* No match detected */ }
	});

  plps.forEach((e) => { try {
    	c = e.className.match(/pl([0-9]+)/)[1];
    	e.style.paddingLeft = `${c}%`;
		} catch { /* No match detected */ }
	});

  prpx.forEach((e) => { try {
    	c = e.className.match(/pr-([0-9]+)/)[1];
    	e.style.paddingRight = `${c}px`;
		} catch { /* No match detected */ }
	});

  prps.forEach((e) => { try {
    	c = e.className.match(/pr([0-9]+)/)[1];
    	e.style.paddingRight = `${c}%`;
		} catch { /* No match detected */ }
	});

  mtpx.forEach((e) => { try {
    	c = e.className.match(/mt-([0-9]+)/)[1];
    	e.style.marginTop = `${c}px`;
		} catch { /* No match detected */ }
	});

  mtps.forEach((e) => { try {
    	c = e.className.match(/mt([0-9]+)/)[1];
    	e.style.marginTop = `${c}%`;
		} catch { /* No match detected */ }
	});

  mbpx.forEach((e) => { try {
    	c = e.className.match(/mb-([0-9]+)/)[1];
    	e.style.marginBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  mbps.forEach((e) => { try {
    	c = e.className.match(/mb([0-9]+)/)[1];
    	e.style.marginBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  ptpx.forEach((e) => { try {
    	c = e.className.match(/pt-([0-9]+)/)[1];
    	e.style.paddingTop = `${c}px`;
		} catch { /* No match detected */ }
	});

  ptps.forEach((e) => { try {
    	c = e.className.match(/pt([0-9]+)/)[1];
    	e.style.paddingTop = `${c}%`;
		} catch { /* No match detected */ }
	});

  pbpx.forEach((e) => { try {
    	c = e.className.match(/pb-([0-9]+)/)[1];
    	e.style.paddingBottom = `${c}px`;
		} catch { /* No match detected */ }
	});

  pbps.forEach((e) => { try {
    	c = e.className.match(/pb([0-9]+)/)[1];
    	e.style.paddingBottom = `${c}%`;
		} catch { /* No match detected */ }
	});

  mpx.forEach((e) => { try {
    	c = e.className.match(/m-([0-9]+)/)[1];
    	e.style.margin = `${c}px`;
    } catch { /* No match detected */ }
  });

  mps.forEach((e) => { try {
      c = e.className.match(/m([0-9]+)/)[1];
      e.style.margin = `${c}%`;
    } catch { /* No match detected */ }
  });

  ppx.forEach((e) => { try {
      c = e.className.match(/p-([0-9]+)/)[1];
      e.style.padding = `${c}px`;
    } catch { /* No match detected */ }
  });

  pps.forEach((e) => { try {
      c = e.className.match(/p([0-9]+)/)[1];
      e.style.padding = `${c}%`;
    } catch { /* No match detected */ }
  });


  // Width & Height

  wpx.forEach((e) => { try {
      c = e.className.match(/w-([0-9]+)/)[1];
      e.style.width = `${c}px`;
    } catch { /* No match detected */ }
  });

  wps.forEach((e) => { try {
      c = e.className.match(/w([0-9]+)/)[1];
      e.style.width = `${c}%`;
    } catch { /* No match detected */ }
  });

  hpx.forEach((e) => { try {
      c = e.className.match(/h-([0-9]+)/)[1];
      e.style.height = `${c}px`;
    } catch { /* No match detected */ }
  });

  hps.forEach((e) => { try {
      c = e.className.match(/h([0-9]+)/)[1];
      e.style.height = `${c}%`;
    } catch { /* No match detected */ }
  });


  // Negative Values

  nmxpx.forEach((e) => { try {
    	c = e.className.match(/nmx-([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}px`;
    	e.style.marginRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmxps.forEach((e) => { try {
    	c = e.className.match(/nmx([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}%`;
    	e.style.marginRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmypx.forEach((e) => { try {
    	c = e.className.match(/nmy-([0-9]+)/)[1];
    	e.style.marginTop = `-${c}px`;
    	e.style.marginBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmyps.forEach((e) => { try {
    	c = e.className.match(/nmy([0-9]+)/)[1];
    	e.style.marginTop = `-${c}%`;
    	e.style.marginBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  npxpx.forEach((e) => { try {
    	c = e.className.match(/npx-([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}px`;
    	e.style.paddingRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  npxps.forEach((e) => { try {
    	c = e.className.match(/npx([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}%`;
    	e.style.paddingRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  npypx.forEach((e) => { try {
    	c = e.className.match(/npy-([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}px`;
    	e.style.paddingBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  npyps.forEach((e) => { try {
    	c = e.className.match(/npy([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}%`;
    	e.style.paddingBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmlpx.forEach((e) => { try {
    	c = e.className.match(/nml-([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmlps.forEach((e) => { try {
    	c = e.className.match(/nml([0-9]+)/)[1];
    	e.style.marginLeft = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmrpx.forEach((e) => { try {
    	c = e.className.match(/nmr-([0-9]+)/)[1];
    	e.style.marginRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmrps.forEach((e) => { try {
    	c = e.className.match(/nmr([0-9]+)/)[1];
    	e.style.marginRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nplpx.forEach((e) => { try {
    	c = e.className.match(/npl-([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nplps.forEach((e) => { try {
    	c = e.className.match(/npl([0-9]+)/)[1];
    	e.style.paddingLeft = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nprpx.forEach((e) => { try {
    	c = e.className.match(/npr-([0-9]+)/)[1];
    	e.style.paddingRight = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nprps.forEach((e) => { try {
    	c = e.className.match(/npr([0-9]+)/)[1];
    	e.style.paddingRight = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmtpx.forEach((e) => { try {
    	c = e.className.match(/nmt-([0-9]+)/)[1];
    	e.style.marginTop = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmtps.forEach((e) => { try {
    	c = e.className.match(/nmt([0-9]+)/)[1];
    	e.style.marginTop = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmbpx.forEach((e) => { try {
    	c = e.className.match(/nmb-([0-9]+)/)[1];
    	e.style.marginBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nmbps.forEach((e) => { try {
    	c = e.className.match(/nmb([0-9]+)/)[1];
    	e.style.marginBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nptpx.forEach((e) => { try {
    	c = e.className.match(/npt-([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}px`;
		} catch { /* No match detected */ }
	});

  nptps.forEach((e) => { try {
    	c = e.className.match(/npt([0-9]+)/)[1];
    	e.style.paddingTop = `-${c}%`;
		} catch { /* No match detected */ }
	});

  npbpx.forEach((e) => { try {
    	c = e.className.match(/npb-([0-9]+)/)[1];
    	e.style.paddingBottom = `-${c}px`;
		} catch { /* No match detected */ }
	});

  npbps.forEach((e) => { try {
    	c = e.className.match(/npb([0-9]+)/)[1];
    	e.style.paddingBottom = `-${c}%`;
		} catch { /* No match detected */ }
	});

  nmpx.forEach((e) => { try {
    	c = e.className.match(/nm-([0-9]+)/)[1];
    	e.style.margin = `-${c}px`;
    } catch { /* No match detected */ }
  });

  nmps.forEach((e) => { try {
      c = e.className.match(/nm([0-9]+)/)[1];
      e.style.margin = `-${c}%`;
    } catch { /* No match detected */ }
  });

  nppx.forEach((e) => { try {
      c = e.className.match(/np-([0-9]+)/)[1];
      e.style.padding = `-${c}px`;
    } catch { /* No match detected */ }
  });

  npps.forEach((e) => { try {
      c = e.className.match(/np([0-9]+)/)[1];
      e.style.padding = `-${c}%`;
    } catch { /* No match detected */ }
  });


  // Negative Width & Height

  nwpx.forEach((e) => { try {
      c = e.className.match(/nw-([0-9]+)/)[1];
      e.style.width = `-${c}px`;
    } catch { /* No match detected */ }
  });

  nwps.forEach((e) => { try {
      c = e.className.match(/nw([0-9]+)/)[1];
      e.style.width = `-${c}%`;
    } catch { /* No match detected */ }
  });

  nhpx.forEach((e) => { try {
      c = e.className.match(/nh-([0-9]+)/)[1];
      e.style.height = `-${c}px`;
    } catch { /* No match detected */ }
  });

  nhps.forEach((e) => { try {
      c = e.className.match(/nh([0-9]+)/)[1];
      e.style.height = `-${c}%`;
    } catch { /* No match detected */ }
  });

};


